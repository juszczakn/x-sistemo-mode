# X-Sistema Modo

https://gitlab.com/juszczakn/x-sistemo-mode

[Ligilo por la Angla.](README.en.md)

[Link for English.](README.en.md)

X-sistemo modo estas lokbufra negrava modo por skribas la specifajn (ĉapelajn) literojn de Esperanto
(ĉ, ĵ, ŝ, ktp).

## Uzi

Vi povas ebligi ĉi tiun modon en bufro per tajpas `M-x x-sistemo-mode`.

Kiam vi tajpas 'x' aŭ 'X' malantaŭ senĉapelo litero, [la modo anstantaŭigas la literon per la Esperantlitero]
(https://eo.wikipedia.org/wiki/X-sistemo).

Ankaŭ, ĝi donas funkcion, `x-sistemo-process-buffer`. Vi povas uzi por ŝangi la tutan bufron.
Ĉu vi forgesis ŝalti la modon, vi povas uzi ĝin por riparas la bufron.

## Instali

Nun, x-sistemo modo ne estas en iu ajn deponejo, sed vi bezonas instali permane.

```
~/$ cd ~/.emacs.d/manual-packages
~/.emacs.d/manual-packages/$ git clone <url>
```

En via `.emacs` aŭ `~/.emacs.d/init.el` dosiero:

```
(add-to-list 'load-path "~/.emacs.d/manual-packages/x-sistemo-mode/")
(require 'x-sistemo-mode)
```
