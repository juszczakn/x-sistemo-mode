# X-Sistemo-Mode

https://gitlab.com/juszczakn/x-sistemo-mode

[Link for Esperanto.](README.eo.md)

[Ligilo por la Esperanta.](README.eo.md)

X-sistemo (x-system) mode is a buffer-local minor-mode for writing Esperanto hatted characters (ĉ, ĵ, ŝ, etc).

## Use

You can enable this mode in a buffer with `M-x x-sistemo-mode`.

As you type, any valid character you type either an 'x' or 'X' afterwards will be replaced (when valid) with
[its corresponding Esperanto-hatted character.](https://en.wikipedia.org/wiki/Esperanto#Alphabet)

It also provides an interactive function, `x-sistemo-process-buffer`, which you can run against a buffer. It will 
replace all valid characters (eg. sx, SX, jX, etc.) with their correct equivalents. So if you forget to turn on
x-sistemo-mode, you can still get the buffer formatted correctly.

## Install

Right now, x-sistemo mode is not available in any package repository, so you'll have to install it manually.

```
~/$ cd ~/.emacs.d/manual-packages
~/.emacs.d/manual-packages/$ git clone <url>
```

Edit your `.emacs` or `~/.emacs.d/init.el` file to have:

```
(add-to-list 'load-path "~/.emacs.d/manual-packages/x-sistemo-mode/")
(require 'x-sistemo-mode)
```
