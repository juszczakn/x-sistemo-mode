;; Eo: x-sistemo negrava modo por solaj bufroj.
;; En: x-system minor mode for individual buffers.
;;
;; https://gitlab.com/juszczakn/x-sistemo-mode


;; Copyright (C) 2017  Nick Juszczak (juszczakn ĉe protonmail.com)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.



(defvar x-sistemo--unhatted-to-hatted-with-x
  (let ((hash (make-hash-table :test 'equal :size 24)))
    (puthash "cx" "ĉ" hash)
    (puthash "cX" "ĉ" hash)
    (puthash "Cx" "Ĉ" hash)
    (puthash "CX" "Ĉ" hash)
    (puthash "gx" "ĝ" hash)
    (puthash "gX" "ĝ" hash)
    (puthash "Gx" "Ĝ" hash)
    (puthash "GX" "Ĝ" hash)
    (puthash "hx" "ĥ" hash)
    (puthash "hX" "ĥ" hash)
    (puthash "Hx" "Ĥ" hash)
    (puthash "HX" "Ĥ" hash)
    (puthash "jx" "ĵ" hash)
    (puthash "jX" "ĵ" hash)
    (puthash "Jx" "Ĵ" hash)
    (puthash "JX" "Ĵ" hash)
    (puthash "sx" "ŝ" hash)
    (puthash "sX" "ŝ" hash)
    (puthash "Sx" "Ŝ" hash)
    (puthash "SX" "Ŝ" hash)
    (puthash "ux" "ŭ" hash)
    (puthash "uX" "ŭ" hash)
    (puthash "Ux" "Ŭ" hash)
    (puthash "UX" "Ŭ" hash)
    hash)
  "The mapping of valid hatted chars with 'x's.
Used for postprocessing a buffer.")


(defun x-sistemo-process-buffer ()
  "Process the current buffer as if it is using the x-system.
Replace all valid characters."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "\\([cghjsuCGHJSU][xX]\\)" nil t)
      (let ((hatted-char (gethash (match-string 1) x-sistemo--unhatted-to-hatted-with-x)))
        (replace-match hatted-char nil nil)))))


(defvar x-sistemo--unhatted-to-hatted
  (let ((hash (make-hash-table :test 'equal :size 12)))
    (puthash "c" "ĉ" hash)
    (puthash "C" "Ĉ" hash)
    (puthash "g" "ĝ" hash)
    (puthash "G" "Ĝ" hash)
    (puthash "h" "ĥ" hash)
    (puthash "H" "Ĥ" hash)
    (puthash "j" "ĵ" hash)
    (puthash "J" "Ĵ" hash)
    (puthash "s" "ŝ" hash)
    (puthash "S" "Ŝ" hash)
    (puthash "u" "ŭ" hash)
    (puthash "U" "Ŭ" hash)
    hash)
  "The mapping of valid hatted chars.")


(defun x-sistemo--replace-char (new-char)
  "Replace current char at point with new-char."
  (backward-delete-char 1)
  (insert new-char))

(defun x-sistemo--try-replace-char (x-char)
  "Check if we should replace char-at-point with a hatted EO char.
If so, just do it."
  (let* ((start-point (max 1 (- (point) 1)))
         (end-point (max 1 (point)))
         (potential-char (buffer-substring  start-point end-point))
         (hatted-char (gethash potential-char x-sistemo--unhatted-to-hatted)))
    (if hatted-char
        (x-sistemo--replace-char hatted-char)
      (insert x-char))))

(defun x-sistemo--try-replace-char-lowercase ()
  "Callback for 'x'."
  (interactive)
  (x-sistemo--try-replace-char "x"))

(defun x-sistemo--try-replace-char-uppercase ()
  "Callback for 'X'."
  (interactive)
  (x-sistemo--try-replace-char "X"))

(defvar x-sistemo--mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "x") 'x-sistemo--try-replace-char-lowercase)
    (define-key map (kbd "X") 'x-sistemo--try-replace-char-uppercase)
    map))

(define-minor-mode x-sistemo-mode
  "EO: X-sistemo por la Esperanta.
EN: X-system for Esperanto."
  :lighter " x-sistemo"
  :keymap x-sistemo--mode-map)

(provide 'x-sistemo-mode)
